Change Log
==========

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`__ and
follows standards set forth at
`keepachangelog <http://keepachangelog.com/>`__.

[0.3.2] - 2017-06-29
--------------------

Fixed
^^^^^

-  Fixed bug with improperly reporting the number of updated styles

[0.3.1] - 2017-06-27
--------------------

Changed
^^^^^^^

-  Updated ``adkit-css`` to only update CSS style if image size changed

`0.3.0 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.3.0%0D0.2.0>`__ - 2017-04-20
-------------------------------------------------------------------------------------------------------

Added
^^^^^

-  HTML5 ad support
-  ``adkit-css`` to generate CSS for image assets
-  ``adkit-optimize`` to optimize all images
-  ``adkit-zip`` to zip all HTML5 ads for delivery
-  ``adkit-upload`` to automatically upload HTML5 ad previews for client
   review

Removed
^^^^^^^

-  Flash support
-  Removed overly complex command line params

Changed
^^^^^^^

-  Updated README file to cover the new additions

`0.2.0 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.2.0%0D0.1.6>`__ - 2015-06-16
-------------------------------------------------------------------------------------------------------

Added
^^^^^

-  Config file for easy adkit usage without needing to specify params
   via command line. Any config params can still be overridden using the
   command line options.
-  When exporting statics, if the max size is not defined in the
   manifest, assume the max size is 40KB.
-  Progressbar to show progress on time consuming loops.
-  ``adkit-quickstart`` method and templates to create adkit.ini and
   manifest.xlsx
-  '--format' parameter to ``adkit-export`` for flexibility
-  Python 3 support

Removed
^^^^^^^

-  Old method to parse external JSON file for upload settings.

Changed
^^^^^^^

-  Updated README file to cover the new additions.
-  Usage of external module exec\_cmd

Fixed
^^^^^

-  Convert issue from PSD to PNG that required us to run the "crush PNG"
   step.

`0.1.6 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.1.6%0D0.1.5>`__ - 2015-05-14
-------------------------------------------------------------------------------------------------------

Added
^^^^^

-  Added ``adkit-preview`` to generate HTML preview files quickly.

`0.1.5 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.1.5%0D0.1.4>`__ - 2015-04-09
-------------------------------------------------------------------------------------------------------

Added
^^^^^

-  Ability to run ``adkit-export`` multiple times without overwriting
   existing PSDs.

`0.1.4 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.1.4%0D0.1.3>`__ - 2015-04-08
-------------------------------------------------------------------------------------------------------

Removed
^^^^^^^

-  Included bashutils module and now require python-bash-utils package

`0.1.3 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.1.3%0D0.1.2>`__ - 2015-04-08
-------------------------------------------------------------------------------------------------------

Fixed
^^^^^

-  Pip install error related to non-used data files

`0.1.2 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.1.2%0D0.1.1>`__ - 2015-04-08
-------------------------------------------------------------------------------------------------------

Added
^^^^^

-  Ability to handle paths with spaces

`0.1.1 <https://bitbucket.org/tsantor/banner-ad-toolkit/branches/compare/0.1.1%0D0.1.0>`__ - 2015-04-07
-------------------------------------------------------------------------------------------------------

Added
^^^^^

-  Small tweaks

`0.1.0 <https://bitbucket.org/tsantor/banner-ad-toolkit/commits/tag/0.1.0>`__ - 2015-04-03
------------------------------------------------------------------------------------------

Added
^^^^^

-  Initial release
